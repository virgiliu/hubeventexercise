from collections import defaultdict, namedtuple
from dataclasses import dataclass

from models import HubEvent, HubMetrics

class CalculatorBase:
    def __init__(self, session):
        """
        Args:
            session: Database session. See sqlalchemy.org.sessionamker
        """
        self.session = session

class AcceptanceCalculator(CalculatorBase):
    """Handles calculation of acceptance score for hubs"""

    @property
    def acceptance(self):
        """Gets dictionary with acceptance for each hub.


        Acceptance is the number of `order/execute/status/customer/processing` events
        versus `order/execute/status/customer/payment` events.

        Return: dictionary with acceptance for each hub.


        See also AcceptanceCalculator.calc_all_hubs_acceptance.
        """

        # Get all `processing` events
        processing = self.get_processing_events()

        # Get all `payment` events
        payment = self.get_payment_events()

        return self.calc_all_hubs_acceptance(payment=payment, processing=processing)

    def get_processing_events(self):
        """Retrieves all events that have the status `order/execute/status/customer/processing`"""

        return self.session.query(HubEvent).filter_by(name=HubEvent.EventName.processing).all()

    def get_payment_events(self):
        """Retrieves all events that have the status `order/execute/status/customer/payment`"""

        return self.session.query(HubEvent).filter_by(name=HubEvent.EventName.payment).all()

    @staticmethod
    def calc_all_hubs_acceptance(processing, payment):
        """Calculates acceptance for all hubs. See also AcceptanceCalculator.acceptance.

        Args:
            processing: List of Event objects with status `order/execute/status/customer/payment`
            payment: List of Event objects with status `order/execute/status/customer/processing`

        Return: dictionary with acceptance for each hub.
        """

        processing_evts = defaultdict(int)
        payment_evts = defaultdict(int)

        for event in payment:
            payment_evts[event.data['hub_id']] += 1

        for event in processing:
            processing_evts[event.data['hub_id']] += 1

        hub_scores = {}

        for hub_id in processing_evts:

            payments = payment_evts.get(hub_id, 0)

            if payments == 0:
                hub_scores[hub_id] = 0

            else:
                hub_scores[hub_id] = round(payments * 100 / processing_evts[hub_id])

        return hub_scores




class MetricIngester:
    """This class offers convenience methods for storing metrics data"""

    metrics_model = HubMetrics

    def __init__(self, session):
        self.session = session
        self.engine = self.session.get_bind()
        if not self.metrics_table_exists():
            print("Metrics table DOES NOT EXIST. Creating table with name: {}".format(
                self.metrics_model.__tablename__))

            self.create_metrics_table()


    def metrics_table_exists(self):
        """Checks if metrics table exists in the database.

        See self.metrics_model

        Returns:
            True if table exists, False otherwise
        """
        return self.engine.dialect.has_table(self.engine, self.metrics_model.__tablename__)


    def create_metrics_table(self):
        """Creates metrics table using self.metrics_model.__tablename__ as the table name"""

        self.metrics_model.__table__.create(bind=self.engine)

    def bulk_insert(self, insert_data):
        """Bulk inserts data from a list.

        Args:
            insert_data: List of dicts to be bulk inserted. Example sturcture:
                    [dict(
                        hub_id=98551,
                        metric='payment_total',
                        value=1575),
                     dict(
                        hub_id=98551,
                        metric='average_payment_per_order',
                        value=36.3
                    )]
        """
        self.engine.execute(self.metrics_model.__table__.insert(), insert_data)


    def save_acceptance_metrics(self, acceptance_data):
        """Saves acceptance data to metrics table.

        Args:
            acceptance_data: Dict. Expected structure:
                             {'hub_id1': 'acceptance_val1', 'hub_id2': 'val2', ...}
        """

        metric = 'acceptance_ratio'
        insert_data = [
            dict(hub_id=hub_id, metric=metric, value=value)
            for hub_id, value in acceptance_data.items()
        ]

        self.bulk_insert(insert_data)

    def save_payment_metrics(self, payment_data):
        """Saves payment metrics to the hub metrics table.

        Args:
            payment_data: Dict. Expected structure:
                          {'hub_id1': HubPayments(), 'hub_id2': HugPayments(), ...}
        """


        insert_data = []
        for hub_id, payment in payment_data.items():
            insert_data.append(dict(
                hub_id=hub_id,
                metric='payment_total',
                value='{:.2f}'.format(payment.total_paid)
            ))

            insert_data.append(dict(
                hub_id=hub_id,
                metric='average_pay_per_order',
                value='{:.2f}'.format(payment.avg_pay_per_order)
            ))

        self.bulk_insert(insert_data)



    def save_rating_metrics(self, hub_score):
        """Saves rating data to metrics table.

        The following metrics will be stored:
        - print_quality_rating
        - service_rating
        - communication_rating
        - speed_rating
        - average_rating


        Args:
            hub_score: HubScore object
        """

        hub_id = hub_score.hub_id

        insert_data = []

        insert_data.append(
            dict(
                hub_id=hub_id,
                metric='average_rating',
                value=hub_score.avg_rating)
        )

        insert_data.append(
            dict(
                hub_id=hub_id,
                metric='print_quality_rating',
                value=hub_score.print_quality)
        )

        insert_data.append(
            dict(
                hub_id=hub_id,
                metric='service_rating',
                value=hub_score.service)
        )

        insert_data.append(
            dict(
                hub_id=hub_id,
                metric='communication_rating',
                value=hub_score.communication)
        )

        insert_data.append(
            dict(
                hub_id=hub_id,
                metric='speed_rating',
                value=hub_score.speed)
        )

        self.bulk_insert(insert_data)



class HubScore:
    """Stores avg scores for a single hub"""

    def __init__(self, hub_id, speed=None, print_quality=None, communication=None, service=None): #pylint: disable=R0913
        self.hub_id = hub_id
        self.speed = speed
        self.print_quality = print_quality
        self.communication = communication
        self.service = service

        self.update_avg_rating()

    def __repr__(self):
        return "<HubScore(hub_id='{}', avg_rating={}, speed={}, print_quality={}, communication={}, service={})>".format( #pylint: disable=C0301
            self.hub_id, self.avg_rating, self.speed, self.print_quality,
            self.communication, self.service)

    def __eq__(self, other):
        return (self.hub_id == other.hub_id
                and self.avg_rating == other.avg_rating
                and self.speed == other.speed
                and self.print_quality == other.print_quality
                and self.communication == other.communication
                and self.service == other.service)

    def __setattr__(self, name, value):
        super().__setattr__(name, value)

        if name in ['speed', 'print_quality', 'communication', 'service']:
            self.update_avg_rating()

    def update_avg_rating(self):
        self.avg_rating = self.calc_avg_rating()

    def calc_avg_rating(self):
        """Calculates the average of the following speed, print quality,
        communication and service metrics.

        If a metric has no score, it will be ignored for the calculation.
        This is to ensure that metrics that have not yet been rated will
        not skew the average rating.
        """

        # TODO: use statistics.mean() instead
        rating = 0
        denom = 0

        # Check for score attributes because calc_avg will be called
        # by __setattr__ during initialization

        if hasattr(self, 'speed') and self.speed is not None:
            rating += self.speed
            denom += 1

        if hasattr(self, 'print_quality') and self.print_quality is not None:
            rating += self.print_quality
            denom += 1

        if hasattr(self, 'communication') and self.communication is not None:
            rating += self.communication
            denom += 1

        if hasattr(self, 'service') and self.service is not None:
            rating += self.service
            denom += 1


        if rating == 0 or denom == 0:
            return None

        return round(rating/denom)

    def update_ratings(self, speed, quality, communication, service):
        """Updates current ratings by averaging the exiting values and input values

        Args that have a value of None will be ignored, but all args must be supplied, just because.

        Args:
            speed: The speed review value
            quality: The printing quality review value
            communication: The communication review value
            service: The service review value
        """

        # TODO: use mapping
        if speed is not None:
            if self.speed is None:
                self.speed = speed
            else:
                self.speed = round((self.speed + speed)/2)

        if quality is not None:
            if self.print_quality is None:
                self.print_quality = quality
            else:
                self.print_quality = round((self.print_quality + quality)/2)

        if communication is not None:
            if self.communication is None:
                self.communication = communication
            else:
                self.communication = round((self.communication + communication)/2)

        if service is not None:
            if self.service is None:
                self.service = service
            else:
                self.service = round((self.service + service)/2)

class ScoreCalculator(CalculatorBase):
    """Handles score calculation for a collection of hub events"""

    @property
    def scores(self):
        """Gets the scores for all hubs in the database.

        Returns:
            Dictionary with scores for each hub.
        """

        review_event_types = [
            HubEvent.EventName.review_created,
            HubEvent.EventName.review_updated,
            HubEvent.EventName.review_deleted
        ]

        review_events = self.session.query(HubEvent) \
                            .filter(HubEvent.name.in_(review_event_types)) \
                            .order_by(HubEvent.timestamp).all()

        scores = ScoreCalculator.calc_scores(review_events)

        return scores




    @staticmethod
    def calc_scores(hub_events):
        """Calculates scores for all hub and averages them.

        Args:
            hub_events: List of `HubEvent` objects
                        NOTE: List must ordered by HubEvent.timestamp

        Returns:
            Dictionary containing scores for each hub.
            Example: {114: HubScore(114, speed=40, service=100), ...}
        """

        # Use dict for faster searching
        hub_scores = {}

        # This is the desired format for storing the hub info
        # hub_scores = {
        #     'hub_id1': {'order_id1': ['evt1', 'evt2'], 'order_id2': ['evt3', 'evt4']},
        #     'hub_id2': {...},
        #     ...
        # }

        # For each hub create a dict of orders
        for event in hub_events:

            # Ensure the hub is defined in scores
            if event.hub_id not in hub_scores:
                hub_scores[event.hub_id] = {}


            # If the review action is 'delete', delete the order's event list
            # from the hub info.
            if event.name == HubEvent.EventName.review_deleted:
                # Pop the order list, discard value
                hub_scores[event.hub_id].pop(event.order_id, None)

            # If the review action is 'create' or 'update', append the action
            else:
                # Ensure that each order has its own list of events
                if event.order_id not in hub_scores[event.hub_id]:
                    hub_scores[event.hub_id][event.order_id] = []

                # Add the review event to the order's list
                hub_scores[event.hub_id][event.order_id].append(event)



        output = {}
        for hub_id, review_events in hub_scores.items():
            output[hub_id] = ScoreCalculator.calc_hub_score(hub_id, review_events)

        return output


    @staticmethod
    def calc_hub_score(hub_id, review_events):
        """Calculates score for a single hub based on reviews from orders.

        Args:
            hub_id: The hub ID.
            review_events: A dictionary holding order IDs and review events.
            Should follow this format:
            {
                'order_id1': ['evt1', 'evt2'],
                'order_id2': ['evt3', 'evt4']
            }

        Returns:
            HubScore object
        """
        score = HubScore(hub_id)
        for _, events in review_events.items():
            score.update_ratings(*ScoreCalculator.calc_order_scores(events))

        return score

    @staticmethod
    def calc_order_scores(order_events):
        """Calculates the review scores of an order for each metric.

        Args:
            order_events: Event list for a single order, ordered by timestmap

        Returns:
            Tuple. (speed, printing_quality, communication, service)
            If a score is not calculated due to missing data, its value
            will be None.
        """

        quality = None
        speed = None
        communication = None
        service = None

        # Go through each event
        for event in order_events:

            #TODO: Use mapping below instead of manually overwriting each value

            # If event has a quality score overwrite existing value
            if event.print_quality_score is not None:
                quality = int(event.print_quality_score)

            # If event has a service score overwrite existing value
            if event.service_score is not None:
                service = int(event.service_score)

            # If event has a speed score overwrite existing value
            if event.speed_score is not None:
                speed = int(event.speed_score)

            # If event has a communication score overwrite existing value
            if event.communication_score is not None:
                communication = int(event.communication_score)

        return (speed, quality, communication, service)



class HubPayments:
    def __init__(self, hub_id, total_paid=0, order_count=0):
        self.hub_id = hub_id
        self.order_count = int(order_count)
        self.total_paid = float(total_paid)


    def __repr__(self):
        return "<HubPayment(hub_id={}, total_paid={}, order_count={}, avg_pay_per_order={})>".format(
            self.hub_id, self.total_paid, self.order_count, self.avg_pay_per_order
        )

    def __eq__(self, other):
        return (self.hub_id == other.hub_id
                and self.total_paid == other.total_paid
                and self.order_count == other.order_count)


    @property
    def avg_pay_per_order(self):
        if self.order_count == 0:
            return 0

        return float(self.total_paid) / int(self.order_count)


class PaymentCalculator(CalculatorBase):
    """Handles calculations related to payment amounts"""

    @property
    def payment_metrics(self):
        events = self.session.query(HubEvent).filter_by(name=HubEvent.EventName.printing).all()
        return PaymentCalculator.calc_payment_metrics(events)


    @staticmethod
    def calc_payment_metrics(events):
        """Calculates payment metrics from a list of events.

        Only events that have the `order/execute/status/customer/printing`
        status will be taken into account.

        Args:
            events: list of HubEvents

        Returns:
            Dict of metrics.
            Example: {'hub_id1': HubPayments(), 'hub_id2': HugPayments(), ...}
        """

        output = {}

        for event in events:
            # Only count orders that have been paid and have started printing
            # TODO: is this check really needed? can we assume the data is already filtered?
            if event.name == HubEvent.EventName.printing:
                hub_id = event.hub_id
                if hub_id not in output:
                    output[hub_id] = HubPayments(event.hub_id, total_paid=event.price_customer, order_count=1)
                else:
                    output[hub_id].total_paid += float(event.price_customer)
                    output[hub_id].order_count += 1

        return output
