import timeit

def time_main():
    setup = 'from main import main'
    stmt = 'main()'
    number = 30

    print(
        'Setup:\n{}\n\nCode to execute:\n{}\n\nNumber of iterations: {}'.
        format(setup, stmt, number),
        flush=True)


    exec_time = timeit.timeit(setup=setup, stmt=stmt, number=number)

    print('Average time per execution: {}. Total execution time: {} seconds.'.format(
        exec_time/number, exec_time
    ))


if __name__ == '__main__':
    time_main()
