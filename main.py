from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from config import user, password, host
from utils import MetricIngester, AcceptanceCalculator, ScoreCalculator, PaymentCalculator

def main():
    engine = create_engine("postgresql://{}:{}@{}".format(user, password, host))

    Session = sessionmaker(bind=engine) #pylint: disable=C0103
    session = Session()

    try:
        print('Connected to DB')

        # Used to insert metrics data
        metrics = MetricIngester(session)

        print('Calculating acceptance')
        # Acceptance ratio
        acceptance_calc = AcceptanceCalculator(session)
        metrics.save_acceptance_metrics(acceptance_calc.acceptance)

        print('Calculating hub ratings')
        # Ratings/scores
        score_calc = ScoreCalculator(session)
        hub_scores = score_calc.scores

        for _, score in hub_scores.items():
            metrics.save_rating_metrics(score)

        print('Calculating payment metrics')
        # Payments metrics
        pay_calc = PaymentCalculator(session)
        metrics.save_payment_metrics(pay_calc.payment_metrics)

        print('Done')



    except:
        raise

    finally:
        session.close()


if __name__ == '__main__':
    main()
