import datetime

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, DateTime, Integer
from sqlalchemy.dialects import postgresql

Base = declarative_base()

# Stop pylint from complaining about too few public methods
# pylint: disable=R0903
class HubEvent(Base):
    """Mapping for hub events table"""
    __tablename__ = 'MY_TABLE'

    id_ = Column('id', String, primary_key=True)
    timestamp = Column(DateTime)
    name = Column(String)
    data = Column(postgresql.JSONB)

    class EventName:
        ## NOTE: The `status`` & `customer` have swapped position in the docs
        ## compared to sample DB data

        processing = 'order/execute/customer/status/processing'
        payment = 'order/execute/customer/status/payment'
        printing = 'order/execute/customer/status/printing'
        successful = 'order/execute/customer/status/successful'
        review_created = 'node/review/created'
        review_updated = 'node/review/updated'
        review_deleted = 'node/review/deleted'

    def __repr__(self):
        return "<HubEvent(id_='{}', timestamp='{}', name='{}', data='{}'>".format(
            self.id_, self.timestamp, self.name, str(self.data))

    @property
    def hub_id(self):
        return self.data['hub_id']

    @property
    def order_id(self):
        return self.data['order_id']

    @property
    def print_quality_score(self):
        return self.data.get('review_value_print_quality', None)

    @property
    def service_score(self):
        return self.data.get('review_value_service', None)

    @property
    def speed_score(self):
        return self.data.get('review_value_speed', None)

    @property
    def communication_score(self):
        return self.data.get('review_value_communication', None)

    @property
    def price_customer(self):
        return self.data.get('price_customer', None)


class HubMetrics(Base):
    """Mapping for hub metrics table"""
    __tablename__ = 'hubscore_metrics'

    id_ = Column('id', Integer, primary_key=True)
    calculated_at = Column(DateTime, default=datetime.datetime.utcnow)
    hub_id = Column(Integer)
    metric = Column(String)
    value = Column(String)

    def __repr__(self):
        return "<HubScoreMetrics(id_='{}', calculated_at='{}', hub_id='{}', metric='{}', value='{}')>".format( #pylint: disable=C0301
            self.id_, self.calculated_at, self.hub_id, self.metric, self.value
        )
