## Requirements

Required software: Python 3, pip, Virtualenv, PostgreSQL 10

Run `pip install -r requirements.txt` to install the necessary Python modules.


In the database you'll need to import the sample data in a table called `MY_TABLE`.

## Configuration

Copy or rename `config.sample.py` to `config.py` and fill in your database credentials.

## Running the program

Enable your virtualenv if you're using one: `source ./.pyenv/bin/activate` (path might differ depending on how you set up your virtualenv)

Run `python main.py`


## Unit tests

Basic unit tests are provided to ensure calculations are correct, using on some dummy data.

To run the tests execute `pytest tests.py`

## Naming

In the code and comments the terms "score", "rating", and "review" are used interchangeably.


## Exported metrics

The metrics exported are as follows:

* speed rating
* print quality rating
* communication rating
* service rating
* average rating: average of above metrics
* payment_total: total payments made via a hub. Only paid orders are taken into consideration
* average_pay_per_order: (payment_total / number of paid orders) for a hub


## Assumptions

### Case 1:
* user creates review for order X with speed rating of 100
* user deletes review for order X
* user creates new review for order X with no speed rating specified, quality rating 100

**Assumption 1**: Only the ratings from latest created review are taken into account. For above case this means: speed rating = NULL, quality rating = 100

### Case 2:
* user creates review for order Y, speed rating of 90
* user updates review for order Y, doesn't specify speed score

**Assumption 2**: For metrics calculation, use the speed score specified when review was created, even though at review update it wasn't specified. For above case this means: speed rating = 90

### Case 3:
* user creates review for order Z with print quality rating of 50
* user deletes review for order Z

**Assumption 3**: Review score for order Z is not taken into consideration.


### Other assumptions

**Assumption 4**: The metrics calculation is not considered to be a mission critical process

**Assumption 5**: The average_rating metric is the average of all the review metrics (speed, quality, communication, service).

**Assumption 6**: In the sample data there are reviews created for orders that don't have any processing/payment/delivered events. Assuming this is correct and that the rest of the data might exist somewhere else.

**Assumption 7**: Event names in the docs don't match those in the DB. Example: "order/execute/customer/status/processing" vs "order/execute/status/customer/processing" -- "status" and "customer" positions are swapped. Will consider event names from sample data to be the correct ones instead of those from docs.

**Assumption 8**: Decimals are not important, values can been rounded, except for payment metrics
