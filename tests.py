import unittest
import uuid
from collections import namedtuple


from models import HubEvent
from utils import AcceptanceCalculator, ScoreCalculator, HubScore, HubPayments, PaymentCalculator

class TestAcceptance(unittest.TestCase):

    def setUp(self):
        HubData = namedtuple('HubData', 'hub_id, processing, payment')

        # Instructions for generating dummy data
        generation_data = [
            HubData(4450, 8, 4), # 50%
            HubData(258, 10, 10), # 100%
            HubData(122, 17, 0), # 0%
            HubData(9981, 9, 3), # 33%
        ]

        self.data = {'processing': [], 'payment': []}

        # Populate dummy data
        for gen_dat in generation_data:

            # `processing` events
            for _ in range(gen_dat.processing):
                self.data['processing'].append(
                    HubEvent(
                        id_=str(uuid.uuid1()),
                        data={'hub_id': gen_dat.hub_id},
                        name=HubEvent.EventName.processing
                    )
                )

            # `payment` events
            for _ in range(gen_dat.payment):
                self.data['payment'].append(
                    HubEvent(
                        id_=str(uuid.uuid1()),
                        data={'hub_id': gen_dat.hub_id},
                        name=HubEvent.EventName.payment
                    )
                )


    def test_acceptance(self):
        result = AcceptanceCalculator.calc_all_hubs_acceptance(
            self.data['processing'], self.data['payment']
        )
        assert result[4450] == 50
        assert result[258] == 100
        assert result[122] == 0
        assert result[9981] == 33


class TestScores(unittest.TestCase):
    def setUp(self):
        self.data = [
            # Order score: service 80
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 35,
                    'hub_id': 116,
                    'review_value_service': 80
                }),

            # Order score: speed 100 quality 15
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 15,
                    'hub_id': 116,
                    'review_value_speed': 100
                }),
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_updated,
                data={
                    'order_id': 15,
                    'hub_id': 116,
                    'review_value_print_quality': 15,
                    'review_value_speed': None
                }),

            # Order score: None, review was deleted
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 23,
                    'hub_id': 116,
                    'review_value_print_quality': 100,
                    'review_value_speed': 100
                }),
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_deleted,
                data={
                    'order_id': 23,
                    'hub_id': 116,
                }),

            # Order score: speed 50 quality 90 communication 100
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 25,
                    'hub_id': 116,
                    'review_value_speed': 100,
                    'review_value_print_quality': 100,
                    'review_value_communication': 100
                }),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_deleted,
                data={
                    'order_id': 25,
                    'hub_id': 116
                }),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 25,
                    'hub_id': 116
                }),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_updated,
                data={
                    'order_id': 25,
                    'hub_id': 116,
                    'review_value_print_quality': 90,
                    'review_value_speed': 10
                }),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_updated,
                data={
                    'order_id': 25,
                    'hub_id': 116,
                    'review_value_speed': 50,
                    'review_value_communication': 100
                }),

            # Order score: quality 100, speed 90, communication None, service None
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 312,
                    'hub_id': 42,
                    'review_value_print_quality': 100,
                    'review_value_speed': 90
                }),

            # Order score: quality None, speed 100, communication None, service None
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.review_created,
                data={
                    'order_id': 315,
                    'hub_id': 42,
                    'review_value_speed': 100
                }),
        ]

    def test_review_scores(self):
        # Hubs should have the following ratings:
        # hub 116: speed 75, quality: 52, communication 100, service 80
        # hub 42: speed 95, quality: 100, communication None, service None

        expected_result = {
            116: HubScore(hub_id=116, speed=75, print_quality=52, communication=100, service=80),
            42: HubScore(hub_id=42, speed=95, print_quality=100)
        }

        scores = ScoreCalculator.calc_scores(self.data)

        assert len(scores) == 2

        for _, hub_score in scores.items():
            print(_)
            assert hub_score == expected_result[hub_score.hub_id]



class TestPayments(unittest.TestCase):
    def setUp(self):
        # Total amount per hub after users have paid:
        # Hub 142: 195.92
        # Hub 999: 80.1
        # Hub 4423: 0

        # Average paid per order per hub:
        # Hub 142: 97.96
        # Hub 999: 26.7
        # Hub 4423: 0


        # Hub 142
        self.data = [
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.printing,
                data={'hub_id': 142, 'price_customer': 50.50}),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.payment,
                data={'hub_id': 142, 'price_customer': 120.5}),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.processing,
                data={'hub_id': 142, 'price_customer': 99.5}),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.printing,
                data={'hub_id': 142, 'price_customer': 145.42}),

            # Hub 999
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.processing,
                data={'hub_id': 999, 'price_customer': 99.5}),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.printing,
                data={'hub_id': 999, 'price_customer': 15.50}),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.printing,
                data={'hub_id': 999, 'price_customer': 5}),

            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.printing,
                data={'hub_id': 999, 'price_customer': 59.6}),

            # Hub 4223
            HubEvent(
                id_=str(uuid.uuid1()),
                name=HubEvent.EventName.processing,
                data={'hub_id': 4423, 'price_customer': 155.9}),
        ]

    def test_payment_metrics(self):
        expected_data = {
            142: HubPayments(142, 195.92, 2),
            999: HubPayments(999, 80.1, 3),
        }

        payment_metrics = PaymentCalculator.calc_payment_metrics(self.data)

        assert len(payment_metrics) == len(expected_data)

        for hub_id, metrics in payment_metrics.items():
            assert metrics == expected_data[hub_id]

        assert payment_metrics[142].avg_pay_per_order == 97.96
        assert payment_metrics[999].avg_pay_per_order == 26.7



# TODO: create tests for HubScore
